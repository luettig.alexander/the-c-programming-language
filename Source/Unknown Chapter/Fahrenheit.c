#include <stdio.h>

/*
    print Fahrenheit-Celsius Table
    for fahr= 0, 20, 40, ..., 300

    celsius = (5.0/9.0) * (fahr - 32.0);
    fahr = celsius * (9.0 / 5.0) + 32;
*/

int main()
{
    float fahr, celsius;
    int lower, upper, step;

    printf("Fahrenheit\tCelsius\n");

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;

    while (fahr <= upper) {
        celsius = (5.0 / 9.0) * (fahr - 32.0);
        printf("%10.0f\t%7.1f\n", fahr, celsius);
        fahr += step;
    }

    return 0;
}
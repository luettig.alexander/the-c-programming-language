#include <limits.h>
#include <float.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Char Range is from %d to %d \n",CHAR_MIN, CHAR_MAX);
    printf("Short Range is from %d to %d \n",SHRT_MIN, SHRT_MAX);
    printf("Int Range is from %d to %d \n",INT_MIN, INT_MAX);
    printf("Long Range is from %ld to %ld \n",LONG_MIN, LONG_MAX);
    return 0;
}

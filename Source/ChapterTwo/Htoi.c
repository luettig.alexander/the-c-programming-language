#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>

int htoi(char s[]);

int main(int argc, char const *argv[])
{
    char hex[] = "FF";
    int result = htoi(hex);

    printf("Result is: %d", result);    
}

int htoi(char s[]){
    int result = 0;
    int size = strlen(s) - 1;
    int count = 0;

    int i = 0;
    if(s[0] == '0' && tolower(s[1]) == 'x'){
        i = 2;
    }

    for(; size >= i && size != -1; size--){
        char c = tolower(s[size]);
        int temp = 0;

        if(c >= '0' && c <= '9'){
            temp += c - '0';
        }
        else if (c >= 'a' && c <='f') {
            temp += c - 'a' + 10;
        }

        result += pow(16, count) * temp;
        count++;
    }
        

    return result;
}
